#!/usr/bin/env bash

for file in $@ 
do
    FullText="$(cat $file)"
    Head="$(head $file)"
    Tail="$(tail $file)"
    HeadLess=${FullText##"$Head"}
    Tailless=${HeadLess%%"$Tail"}
    
    echo "$file"
    echo "$Tailless"
done